import React, {useState, useEffect} from 'react'
import 'antd/dist/antd.css';
import { Modal, AutoComplete, Input, Button, InputNumber, Form } from 'antd';
import { useHistory, useParams } from "react-router-dom";
import axios from 'axios';

const layout = {
  labelCol: {
    span: 8,
  },
  wrapperCol: {
    span: 16,
  },
};

function EditModal(props) {
  let history = useHistory();
  const [places, setPlaces] = useState({...props.place});
  const [options, setOptions] = useState([]);
  const [localoptions, setlocalOptions] = useState([]);
   const[zone, setZone] = useState("");
  const[zonename, setZoneName] = useState("");
  const[regioncode, setRegionCode] = useState("");
  const[regionname, setRegionName] = useState("");

  useEffect ( async() => {
    const result = await axios.get(`https://api.stage.raaho.in/places2/v2/zones/`);
    setOptions(result.data.data)
    setPlaces(props.place)
}, [props.place])

useEffect(async() => {
  const result = await axios.get(`https://api.stage.raaho.in/places2/v2/zones/`);
  places.secondaryZoneCode=zone;
  places.secondaryZoneName=zonename;
  places.regionCode= regioncode;
  places.regionName=regionname;
  setPlaces({...places})
}, [zone])

const handleSearch = async (value) => {
    const tempOptions = options.filter( (item) => {
        return item.zoneCode.toLowerCase().includes(value.toLowerCase())
        
    })
    .map((item, index) => {
        return{
            label: item.zoneCode,
            value: item.zoneCode,
            id: item.zoneId,
        };
    });
    setlocalOptions(tempOptions);
  };

  const onSelect = (value, option) => {
    const tempPlace = options.find( (item) => {
      return item.zoneId === option.id
      
  })
    console.log(tempPlace, "temp is")
    setZone(tempPlace.zoneCode)
    setZoneName(tempPlace.zoneName)
    setRegionCode(tempPlace.regionCode)
    setRegionName(tempPlace.regionName)
    
  };

  const onInputChange = (e) => {
  setPlaces({...places, [e.target.name]: e.target.value})
  }

  console.log(localoptions, "options")
  const onFinish = async(e) => {
    // e.preventDefault();
    // await axios.put(`https://api.stage.raaho.in/places2/v2/places/${props.selectedPlaceId}`, places);
    // history.push("/");
    console.log(places, "places")
  }
    return (
        <div>
        <Modal
        visible= {props.visible}
         title="Edit"
         onOk={props.onOk}
         onCancel={props.onCancel}
         confirmLoading={props.confirmLoading}
      >
        <Form {...layout} name="nest-message" >

      <Form.Item
        name={['place', 'name']}
        label="Place Name"
      >
        <Input placeholder='Edit place name' name="placeName" defaultValue={places.placeName} onChange={e => onInputChange(e)}/>
      </Form.Item>

      <Form.Item
        name={['place', 'latitude']}
        label="Place Latitude"
      >
        <InputNumber placeholder='Edit place latitude' defaultValue={places.placeLatitude} name="placeLatitude" onChange={e => onInputChange(e)} />
      </Form.Item>

      <Form.Item
        name={['place', 'longitude']}
        label="Place Longitude"
      >
        <InputNumber placeholder='Edit place longitude' name="placeLongitude" defaultValue={places.placeLongitude} onChange={e => onInputChange(e)}/>
      </Form.Item>

      <Form.Item name={['place', 'prizonecode']} 
      label="Primary Zone Code" 
      >
        <Input placeholder='Edit primary zone code' name="primaryZoneCode" defaultValue={places.primaryZoneCode} onChange={e => onInputChange(e)}/>
      </Form.Item>

      <Form.Item name={['place', 'prizonename']} 
      label="Primary Zone Name"
      >
        <Input placeholder='Edit primary zone name' name="primaryZoneName"  defaultValue={places.primaryZoneName} onChange={e => onInputChange(e)}/>
      </Form.Item>

      <Form.Item name={['place', 'seczonecode']} 
      label="Secondary Zone Code" 
      > 
      
      <AutoComplete placeholder="Type here" name="secondaryZoneCode" defaultValue={places.secondaryZoneCode} options={localoptions} onSelect={onSelect} onSearch={handleSearch}></AutoComplete>
      </Form.Item>

      <Form.Item name={['place', 'seczonename']} 
      label="Secondary Zone Name" 
      >
        <Input placeholder='Edit secondary zone name' readonly="true" name="SecondaryZoneName" defaultValue={places.secondaryZoneName} onChange={e => onInputChange(e)}/>
      </Form.Item>


      <Form.Item name={['place', 'regioncode']} 
      label="Region Code" 
      >
        <Input  readonly="true" placeholder='Edit regional code' name="regionCode" defaultValue={places.regionCode} onChange={e => onInputChange(e)}/>
      </Form.Item>

      <Form.Item name={['place', 'regionname']} 
      label="Region Name"
      >
        <Input  readonly="true" placeholder='Edit regional namee'name="regionName" defaultValue={places.regionName} onChange={e => onInputChange(e)} />
      </Form.Item>

      <Form.Item name={['place', 'googleplacedId']} 
      label="Google Placed ID"
      >
        <Input readonly="true" defaultValue={props.place.googlePlaceId}/>
      </Form.Item>

      <Form.Item wrapperCol={{ ...layout.wrapperCol, offset: 8 }}>
        <Button type="primary" htmlType="submit" onClick={onFinish}>
          Submit
        </Button>
      </Form.Item>

    </Form>
         </Modal>
        </div>
    )
}

export default EditModal
