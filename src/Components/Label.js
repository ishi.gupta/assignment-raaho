import React, {useState, useEffect} from 'react'
import { Input, AutoComplete, Button, Tooltip } from 'antd';
import "antd/dist/antd.css";
import axios from 'axios'
import { SearchOutlined, EditOutlined, GoogleOutlined } from '@ant-design/icons';
import EditModal from './EditModal'
import { Link } from "react-router-dom";

function Label() {
    const [options, setOptions] = useState([]);
    const [localoptions, setlocalOptions] = useState([]);
    const[visible, setVisible] =  useState(false);
    const [confirmLoading, setConfirmLoading] = useState(false);
    const[place, setPlace] =useState({});
    const[selectedPlaceId, setSelectedPlaceId] = useState("");

    useEffect ( async() => {
        const result = await axios.get(`https://api.stage.raaho.in/places2/v2/places`);
        setOptions(result.data.data[0].places)
    }, [])

    const handleSearch = async (value) => {
        const tempOptions = options.filter( (item) => {
            return item.placeName.toLowerCase().includes(value.toLowerCase())
            
        })
        .map((item, index) => {
            return{
                label: item.placeName,
                value: item.placeName,
                id: item.placeId,
            };
        });
        setlocalOptions(tempOptions);
      };

      const onSelect = (value, option) => {
        const tempPlace = options.find( (item) => {
          return item.placeId === option.id
          
      })
        setPlace(tempPlace)
      };

      const showModal = () => {
          setSelectedPlaceId(place.placeId);
          setVisible(true);
      }

      const handleCancel = () => {
        setVisible(false);
      };

      const handleOk = () => {
        setConfirmLoading(true);
        setTimeout(() => {
          setVisible(false);
          setConfirmLoading(false);
        }, 1000);
      };

      const openMap = () => {
        let url= `http://maps.google.com/maps?q=${place.placeLatitude},${place.placeLongitude}`
        window.open(url, "_blank");
      }
    return (
        <div>
          <div style={{display: "flex", alignItems: "center", justifyContent: "center", flexDirection: "row", marginTop:50}}>
            <AutoComplete style={{width: 200}} placeholder="Type here" options={localoptions} onSelect={onSelect} onSearch={handleSearch}></AutoComplete>
            <Button type="primary" icon={<SearchOutlined />} onClick={onSelect}>
            Search
            </Button>
            </div>
              <div style={{display: "flex", alignItems: "center", justifyContent: "center", flexDirection: "row", marginTop: 50}}>
                 { place.placeId  ? <Button style={{marginRight: 40}} type="primary" icon={<EditOutlined />} onClick={showModal}>Edit</Button>:null }
                 { place.placeId  ? <Button type="primary" icon={<GoogleOutlined />}onClick={openMap}>Open Map</Button>:null }
               </div>            
            <EditModal visible={visible} onCancel={handleCancel} onOk={handleOk} confirmLoading={confirmLoading} place={place} selectedPlaceId={selectedPlaceId}/>
        </div>
    )
}

export default Label
